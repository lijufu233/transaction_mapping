
## A transcation mapping application (SPA) using react  
## react + django 


## To see Dump data 

```bash
backend> python3 manage.py dumpdata api api.Transaction --indent 2 > transaction.json
```


## Using Redux as central data store for app data and manage state 
## fetch data from backend and save in store 



## I am kind understand the logics behind Redux 
## Store: center storage shared by all component 
## reducer: a middleware kind action function triggered by action and then do corresponding operation on the store (update certain state value etc)
## action: has type and content, trigger reducer 



