import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { fetch_marks } from './action';


//// there should be three layers in order to avoid re-rendering when click anything 
/// seems like google-maps-react doesnt allow to separate another sub-compoent eg: componet for maker/ or info-window

/// store holds the complete state of my app 
/// The way to change the state is by dispathing action 


// use pure Google Map Api to do this 
// ref: https://developers.google.com/maps/documentation/javascript/infowindows
// ref: https://developers.google.com/maps/documentation/javascript/markers
// ref: https://developers.google.com/maps/documentation/javascript/tutorial

class App extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.start()
  }

  // use asynchronized to make sure fetch the data, then render the map 


  // add script into our function 
  // credit to Youtube teacher for this part 
  // I was struggling by "Google-map-react"
  start = () => {
    loadScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCWoT8Kpxwz5L6hr3MyFkFOH1Mhcij67_A&callback=initMap")
    window.initMap = this.startMap
  }

  startMap = () => {
    var map = new window.google.maps.Map(document.getElementById('map'), {
      center: { lat: 43.653908, lng: -79.384293 },
      zoom: 13
    })

    var infowindow = new window.google.maps.InfoWindow();

    this.props.marks.map((mark, i) => {

      // store the content of the info window of current window 
      var window_string = "The price is " + mark.price + ", " + "The property type is " + mark.property_type

      // each marker 
      var marker = new window.google.maps.Marker({
        position: { lat: parseFloat(mark.latitude), lng: parseFloat(mark.longitude) },
        map: map
      })

      // add listener to monitor the marker click event 
      marker.addListener('click', function () {
        infowindow.setContent(window_string)
        infowindow.open(map, marker);
      });


    }
    )

  }

  render() {
    //console.log(this.props.marks)
    return (
      <main> <div id="map"></div> </main>
    )
  }



}

const mapStore = (state) => {
  return {
    marks: state.marks
  }
}

// add the google map script into index 
// ref: https://www.youtube.com/watch?v=W5LhLZqj76s, credit to Youtube teacher 
function loadScript(url) {
  var index = window.document.getElementsByTagName("script")[0]
  var script = window.document.createElement("script")
  script.src = url
  script.async = true
  script.defer = true
  index.parentNode.insertBefore(script, index)
}



export default connect(mapStore, fetch_marks)(App);