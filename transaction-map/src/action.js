export function fetch_marks() {
    return (dispatch) => {
        return fetch("http://127.0.0.1:8000/transactions/")
        .then(response => response.json())
        .then(data =>{
            dispatch(get_marks(JSON.parse(JSON.stringify(data))))
        })
    }
}

export function get_marks(marks){
    return {
        type: "FETCH_MARKS",
        marks: marks 
    }
}