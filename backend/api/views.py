from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Transaction
from .serializers import TransactionSerizlizers
from .new_data import create_inital
### use @api.view decorator for function based views 

# Create your views here.


@api_view(['Get'])
def get_transaction(request):
    all_transaction = Transaction.objects.all()
    if(len(all_transaction) < 100):
        create_inital()
    serializer = TransactionSerizlizers(all_transaction, many =True)
    return Response(serializer.data)