from django.db import models

# Create your models here.
## Our transaction models four fields with needed property, same as defined within sqlite table
## latitude decimal(8,6) longitude decimal(9,6)
## dont consider add restrition on price field, will validate when generating 

class Transaction(models.Model):
    price = models.IntegerField()
    property_type = models.CharField(max_length=100)
    latitude = models.DecimalField(max_digits=10, decimal_places=6)
    longitude = models.DecimalField(max_digits=10, decimal_places=6)

    def __str__(self):
        return str(self.price) + " " + self.property_type + " " + str(self.latitude) + " " + str(self.longitude)